# Assignment 2

## Description

### Appendix A
The first part is done using sql queries. You can find the .sql files in the folder [SQLQueries](SQLQueries)

### Appendix B
In the folder CreateDatabase, we are updating adding and reading from a given database through c#.

## Contributors

- Markus Dybedal
- Ole Martin Gjerde
