USE SuperHeroesDb;

CREATE TABLE PowerList(
Superhero_ID int NOT NULL,
Power_ID int NOT NULL

CONSTRAINT pk_heroability PRIMARY KEY(Superhero_ID, Power_ID)
);


ALTER TABLE PowerList
ADD CONSTRAINT fk_superheroability FOREIGN KEY (Superhero_ID) REFERENCES Superhero(Superhero_ID)

ALTER TABLE PowerList
ADD CONSTRAINT fk_powerability FOREIGN KEY (Power_ID) REFERENCES [Power](Power_ID)