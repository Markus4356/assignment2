USE SuperheroesDb;

INSERT INTO Power(PowerName, Description) VALUES ('Fly', 'Can fly')
INSERT INTO Power(PowerName, Description) VALUES ('Invisibility', 'Can become completely invisible')
INSERT INTO Power(PowerName, Description) VALUES ('Strength', 'Superhuman strength')
INSERT INTO Power(PowerName, Description) VALUES ('Laser vision', 'Can shot laser beams from the eyes')

INSERT INTO PowerList
VALUES
(1,1),
(1,3),
(1,4),
(2,1),
(2,3),
(3,2);