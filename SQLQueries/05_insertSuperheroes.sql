USE SuperheroesDb;

INSERT INTO Superhero(SuperheroName, Alias, Origin) VALUES ('Clark Kent', 'Superman', 'Krypton')
INSERT INTO Superhero(SuperheroName, Alias, Origin) VALUES ('Thor Odinson', 'Thor', 'Aasgard')
INSERT INTO Superhero(SuperheroName, Alias, Origin) VALUES ('Susan Richards', 'Invisible woman', 'Long Island')