USE SuperheroesDb;

ALTER TABLE Assistant
ADD Superhero_ID int NOT NULL

ALTER TABLE Assistant
ADD CONSTRAINT fk_superhero
FOREIGN KEY (Superhero_ID) REFERENCES Superhero(Superhero_ID)