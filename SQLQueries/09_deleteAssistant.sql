USE SuperheroesDb;

DELETE FROM Assistant where AssistantName IN
	(
	SELECT AssistantName FROM Assistant
	GROUP BY AssistantName HAVING count(*)=1
	AND AssistantName = 'Johnny'
	)