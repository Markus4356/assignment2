USE SuperHeroesDb;

CREATE TABLE Superhero (
Superhero_ID int PRIMARY KEY IDENTITY(1,1),
SuperheroName nvarchar(100),
Alias nvarchar(100),
Origin nvarchar(100)
);

CREATE TABLE Assistant (
Assistant_ID int PRIMARY KEY IDENTITY(1,1),
AssistantName nvarchar(100)
);

CREATE TABLE Power (
Power_ID int PRIMARY KEY IDENTITY(1,1),
PowerName nvarchar(100),
Description nvarchar(300)
);