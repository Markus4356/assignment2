USE SuperheroesDb;

UPDATE Superhero
SET SuperheroName = 'Thor'
WHERE SuperheroName = 'Thor Odinson'

UPDATE Superhero
SET Alias = 'God of Thunder'
WHERE Alias = 'Thor'