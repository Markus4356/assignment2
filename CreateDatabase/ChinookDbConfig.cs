﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase
{
    public class ChinookDbConfig
    {
        /// <summary>
        /// Sets the string to connect to the database through ssms.
        /// </summary>
        /// <returns>A type of string that has the information 
        /// needed when creating a connection</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "localhost\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;

            return builder.ConnectionString;
        }
    }
}
