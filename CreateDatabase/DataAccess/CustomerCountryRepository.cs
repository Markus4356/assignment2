﻿using CreateDatabase.Interfaces;
using CreateDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.DataAccess
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// This method accesses the customer table and groups them 
        /// by countries.The most populated country is sorted first.
        /// </summary>
        /// <returns>A list with the how many customers from each country 
        /// in a descending manner</returns>
        public List<CustomerCountry> GetCountryList()
        {
            List<CustomerCountry> countrylist = new List<CustomerCountry>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand(
                    "SELECT COUNT(CustomerId) AS NumberOfPeople," +
                    " Country FROM Customer GROUP BY Country " +
                    "ORDER BY COUNT(CustomerID) DESC", connection);

                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    CustomerCountry customerCountry = new CustomerCountry()
                    {
                        Citizens = myReader.GetInt32(0), //This is how many customers live in the country
                        Country = myReader.IsDBNull(1) ? null : myReader.GetString(1), //This is the country
                    };
                    countrylist.Add(customerCountry);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return countrylist;
        }
    }
}
