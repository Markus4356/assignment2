﻿using CreateDatabase.Interfaces;
using CreateDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CreateDatabase.DataAccess
{
    
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Gets the favorite genre for a given customer
        /// based on how many songs the customer has bought from each genres.
        /// </summary>
        /// <param name="id">The id of the customer you are interested in
        /// knowing the favorite genre of</param>
        /// <returns>List of the favorite genre, 
        /// multiple entries in the list in case of tie.</returns>
        public List<CustomerGenre> GetFavoriteGenre(int id)
        {
            List<CustomerGenre> favoriteGenres = new List<CustomerGenre>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand(
                    "SELECT TOP 1 WITH TIES COUNT(Genre.Name), Genre.Name from Invoice " +
                    "JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                    "JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                    "JOIN Genre On Track.GenreId = Genre.GenreId " +
                    "WHERE CustomerId = @CustomerId GROUP BY Genre.Name " +
                    "ORDER BY COUNT(Genre.Name) DESC", connection);

                command.Parameters.AddWithValue("@customerId", id);

                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    CustomerGenre customerGenre = new CustomerGenre()
                    {
                        Id = id,
                        Instances = myReader.GetInt32(0), //number of songs bought from the genre
                        Genre = myReader.IsDBNull(1) ? null : myReader.GetString(1), //The favorite genre
                    };
                    favoriteGenres.Add(customerGenre);
                }                   
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return favoriteGenres;
        }

    }
}
