﻿using CreateDatabase.Interfaces;
using CreateDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.DataAccess
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Calculates the total each customer has spent in total
        /// </summary>
        /// <returns>List with id and total spent in a decreasing manner </returns>
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> spenderList = new List<CustomerSpender>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand(
                    "Select CustomerId , SUM(Total) as TotalSum from Invoice " +
                    "Group By CustomerId ORDER BY TotalSum DESC", connection);

                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    CustomerSpender customerSpender = new CustomerSpender()
                    {
                     Id = myReader.GetInt32(0),

                     TotalSpent = (double)myReader.GetDecimal(1),
                    };
                    spenderList.Add(customerSpender);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return spenderList;
        }
    }
}
