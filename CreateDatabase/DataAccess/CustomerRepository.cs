﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using CreateDatabase.Interfaces;
using CreateDatabase.Models;

namespace CreateDatabase.DataAccess
{
    public class CustomerRepository : ICustomerRepository<Customer>
    {
        /// <summary>
        /// This method access the Customer Table and
        /// creates a list of all Customers with the attributes defined in the Customer Class.
        /// </summary>
        /// <returns>Returns a list of all Customers</returns>
        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand("SELECT * FROM Customer", connection);
                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    Customer customer = new Customer()
                    {
                        Id = myReader.GetInt32(0),
                        FirstName = myReader.IsDBNull(1) ? null : myReader.GetString(1),
                        LastName = myReader.IsDBNull(2) ? null : myReader.GetString(2),
                        Country = myReader.IsDBNull(7) ? null : myReader.GetString(7),
                        PostalCode = myReader.IsDBNull(8) ? null : myReader.GetString(8),
                        PhoneNumber = myReader.IsDBNull(9) ? null : myReader.GetString(9),
                        Email = myReader.IsDBNull(11) ? null : myReader.GetString(11),

                    };
                    customers.Add(customer);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customers;
        }

        /// <summary>
        /// This method access the Customer Table and
        /// creates a list of Customers with a given name.
        /// </summary>
        /// <param name="name">The name of the Customer that will be added to the list</param>
        /// <returns>Returns a list of Customers</returns>
        public List<Customer> GetByName(string name)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand("SELECT * FROM Customer WHERE FirstName LIKE @firstname", connection);

                command.Parameters.AddWithValue("@firstname", name);

                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    Customer customer = new Customer()
                    {
                        Id = myReader.GetInt32(0),
                        FirstName = myReader.IsDBNull(1) ? null : myReader.GetString(1),
                        LastName = myReader.IsDBNull(2) ? null : myReader.GetString(2),
                        Country = myReader.IsDBNull(7) ? null : myReader.GetString(7),
                        PostalCode = myReader.IsDBNull(8) ? null : myReader.GetString(8),
                        PhoneNumber = myReader.IsDBNull(9) ? null : myReader.GetString(9),
                        Email = myReader.IsDBNull(11) ? null : myReader.GetString(11),

                    };
                    customers.Add(customer);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customers;
        }

        /// <summary>
        /// This method access the Customer Table and get a Customer
        /// that corresponds with the given id.
        /// </summary>
        /// <param name="id">The ID of the Customer that we will get.</param>
        /// <returns>Returns a Customer</returns>
        public Customer GetById(int id)
        {
            Customer customer = new Customer();
            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand($"SELECT * FROM Customer WHERE CustomerId = @customerId", connection);

                command.Parameters.AddWithValue("@customerId", id);

                using SqlDataReader myReader = command.ExecuteReader();

                myReader.Read();
                customer = new Customer()
                {
                    Id = myReader.GetInt32(0),
                    FirstName = myReader.IsDBNull(1) ? null : myReader.GetString(1),
                    LastName = myReader.IsDBNull(2) ? null : myReader.GetString(2),
                    Country = myReader.IsDBNull(7) ? null : myReader.GetString(7),
                    PostalCode = myReader.IsDBNull(8) ? null : myReader.GetString(8),
                    PhoneNumber = myReader.IsDBNull(9) ? null : myReader.GetString(9),
                    Email = myReader.IsDBNull(11) ? null : myReader.GetString(11),

                };
                return customer;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customer;
        }

        /// <summary>
        /// This method access the Customer Table and creates a list
        /// of Customers with a limit (how many entries to be added the list)
        /// and a offset (the offset from where we start to add)
        /// </summary>
        /// <param name="limit">the limit of entries we will get</param>
        /// <param name="offset">the offset of which row to start on</param>
        /// <returns>Returns a list of Customers</returns>
        public List<Customer> GetSubset(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();

            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());

                connection.Open();

                using SqlCommand command = new SqlCommand("SELECT * FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY", connection);

                command.Parameters.AddWithValue("@limit", limit);
                command.Parameters.AddWithValue("@offset", offset);

                using SqlDataReader myReader = command.ExecuteReader();

                while (myReader.Read())
                {
                    Customer customer = new Customer()
                    {
                        Id = myReader.GetInt32(0),
                        FirstName = myReader.IsDBNull(1) ? null : myReader.GetString(1),
                        LastName = myReader.IsDBNull(2) ? null : myReader.GetString(2),
                        Country = myReader.IsDBNull(7) ? null : myReader.GetString(7),
                        PostalCode = myReader.IsDBNull(8) ? null : myReader.GetString(8),
                        PhoneNumber = myReader.IsDBNull(9) ? null : myReader.GetString(9),
                        Email = myReader.IsDBNull(11) ? null : myReader.GetString(11),

                    };
                    customers.Add(customer);
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return customers;
        }

        /// <summary>
        /// This method adds a Customer to the Customer Table in the Database
        /// </summary>
        /// <param name="customer">The Customer to be added to the database</param>
        public void AddCustomer(Customer customer)
        {
            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();
                string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) VALUES (@FirstName, @LastName, @Country, @PostalCode, @PhoneNumber, @Email)";

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                command.Parameters.AddWithValue("@LastName", customer.LastName);
                command.Parameters.AddWithValue("@Country", customer.Country);
                command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                command.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);

                command.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// This method updates some attributes of a Customer: Email and Phone
        /// </summary>
        /// <param name="customer">The Customer to be updated</param>
        public void UpdateCustomer(Customer customer)
        {
            try
            {
                using SqlConnection connection = new SqlConnection(ChinookDbConfig.GetConnectionString());
                connection.Open();
                string sql =
                "UPDATE Customer SET Email = @Email," +
                "Phone = @Phone WHERE CustomerId = @CustomerId";

                using SqlCommand command = new SqlCommand(sql, connection);

                command.Parameters.AddWithValue("@CustomerId", customer.Id);
                command.Parameters.AddWithValue("@Phone", customer.PhoneNumber);
                command.Parameters.AddWithValue("@Email", customer.Email);

                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
