﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.Models
{
    public class CustomerSpender
    {
        public int Id { get; set; }
        public double? TotalSpent { get; set; }
    }
}
