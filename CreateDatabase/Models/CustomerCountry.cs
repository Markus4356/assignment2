﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.Models
{
    public class CustomerCountry
    {
        public int Citizens { get; set; }
        public string? Country { get; set; }

    }
}
