﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.Models
{
    public class CustomerGenre
    {
        public int Id { get; set; }
        public int Instances { get; set; }
        public string? Genre { get; set; }
        
    }
}
