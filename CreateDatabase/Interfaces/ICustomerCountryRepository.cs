﻿using CreateDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.Interfaces
{
    public interface ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCountryList();
    }
}
