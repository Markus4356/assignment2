﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateDatabase.Interfaces
{
    public interface ICustomerRepository<T>
    {
        public List<T> GetAll();

        public T GetById(int id);

        public List<T> GetByName(string name);

        public List<T> GetSubset(int limit, int offset);

        public void AddCustomer(T entity);

        public void UpdateCustomer(T entity);

    }
}
