﻿using CreateDatabase.DataAccess;
using CreateDatabase.Models;
using System.Linq;

CustomerRepository customer = new CustomerRepository();
CustomerCountryRepository customerCountry = new CustomerCountryRepository();
CustomerSpenderRepository customerSpender = new CustomerSpenderRepository();
CustomerGenreRepository customerGenre = new CustomerGenreRepository();


//--------------GET ALL CUSTOMERS------------------------
//List<Customer> allCustomers = customer.GetAll();
//foreach (var p in allCustomers)
//{
//    Console.WriteLine($"{p.Id}\t{p.FirstName}\t{p.LastName}\t" +
//        $"{p.Country}\t{p.PostalCode}\t{p.PhoneNumber}\t{p.Email}");
//}

//--------------GET CUSTOMER BY ID------------------------
//Customer myCustomerById = customer.GetById(12);
//Console.WriteLine($"{myCustomerById.Id}\t{myCustomerById.FirstName}\t{myCustomerById.LastName}\t{myCustomerById.Country}\t{myCustomerById.PostalCode}\t{myCustomerById.PhoneNumber}\t{myCustomerById.Email}");


//--------------GET CUSTOMER BY NAME------------------------
//List<Customer> myCustomerByName = customer.GetByName("Fra%");

//foreach (var p in myCustomerByName)
//{
//    Console.WriteLine($"{p.Id}\t{p.FirstName}\t{p.LastName}\t" +
//        $"{p.Country}\t{p.PostalCode}\t{p.PhoneNumber}\t{p.Email}");
//}


//--------------GET CUSTOMER SUBSET------------------------
//List<Customer> myCustomerSubset = customer.GetSubset(10, 10);

//foreach (var p in myCustomerSubset)
//{
//    Console.WriteLine($"{p.Id}\t{p.FirstName}\t{p.LastName}\t{p.Country}\t{p.PostalCode}\t{p.PhoneNumber}\t{p.Email}");
//}

//--------------ADD CUSTOMER------------------------
//Customer customerAdded = new Customer
//{
//    FirstName = "Tom",
//    LastName = "Parker",
//    Country = "Germany",
//    PostalCode = "6723",
//    PhoneNumber = "95156195",
//    Email = "tomparker@hotmail.com",
//};

//customer.AddCustomer(customerAdded);


//--------------UPDATE CUSTOMER------------------------
//Customer customerUpdated = new Customer
//{
//    Id = 60,
//    PhoneNumber = "47156195",
//    Email = "tomparker@gmail.com",
//};

//customer.UpdateCustomer(customerUpdated);


//--------------CUSTOMER COUNTRY------------------------
//List<CustomerCountry> myCustomerCountry = customerCountry.GetCountryList();

//foreach (var p in myCustomerCountry)
//{
//    Console.WriteLine($"{p.Citizens}\t{p.Country}\t");
//}

//--------------CUSTOMER SPENDING------------------------
//List<CustomerSpender> myCustomerSpender = customerSpender.GetHighestSpenders();

//foreach (var p in myCustomerSpender)
//{
//    Console.WriteLine($"{p.Id}\t{p.TotalSpent}\t");
//}


//--------------CUSTOMER GENRE------------------------
//List<CustomerGenre> myCustomerGenre = customerGenre.GetFavoriteGenre(2);

//foreach (var p in myCustomerGenre)
//{
//    Console.WriteLine($"Customer with ID {p.Id} has {p.Genre} as " +
//    $"favorite genre with {p.Instances} {p.Genre}songs bought.");
//}

